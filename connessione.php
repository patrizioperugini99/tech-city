
<?php

  //ritorna numero massimo di persone che possono entrare
  function getMax(){
    return 6;
  }
  //ritorna numero massimo di macchine che possono transitare
  function getTraffico(){
    return 4;
  }
  //ritorna numero di parcheggi
  function getMaxParcheggio(){
    return 20;
  }
  //cancella una prenotazione
  function delete(){
    
    $conn=db_connection();
    
    $query="SELECT p.idPrenotazione 
              FROM Macchina m,Prenotazione p
              WHERE p.idPrenotazione=m.idPrenotazione and m.Transito=0
              GROUP BY p.idPrenotazione
              LIMIT 1";

    $delete_id=$conn->query($query);
    $ris=$delete_id->fetch_array(MYSQLI_ASSOC);
    //id della prenotazione da eliminare
    $deleted=$ris['idPrenotazione'];
    //liberiamo parcheggio
    $query="UPDATE `Parcheggio` SET `occupato`='0' 
            WHERE idParcheggio=(SELECT idParcheggio FROM Macchina where idPrenotazione=$deleted)";
     $set_parcheggio=$conn->query($query);
    if(!$set_parcheggio){
      echo "errore in settare il parcheggio";
      exit(0);
    }

    $query="DELETE from Prenotazione where idPrenotazione=$deleted";
    $delete=$conn->query($query);
    if(!$delete){
      echo "errore in delete";
      exit(0);
    }
    return 1; 
  }
  //esegue la coneesione al db
  function db_connection(){
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "Tech_city";
    // Crea la connessione
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Controllo sulla connessione
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
  
   
    return $conn;

  }
  //ritorna i posti disponibili nel centro storico
  function postiCS(){

    $conn=db_connection();
    $query="SELECT *
            from Parcheggio
            where occupato=1 and idParcheggio>='B0' and idParcheggio<'C0'";
    $rows=$conn->query($query);
    if(!$rows){
      echo "errore ";
        exit(0);
    }
    $num_p=$rows->num_rows;
    $p=getMaxParcheggio();
    $conn->close();
    return $p-$num_p;
   
  }
  //ritorna i posti disponibili nel centro commerciale
  function postiCM(){
    $conn=db_connection();
    $query="SELECT *
            from Parcheggio
            where occupato=1 and idParcheggio>='A0' and idParcheggio<'B0'";
    $rows=$conn->query($query);
    if(!$rows){
      echo "errore ";
        exit(0);
    }
    $num_p=$rows->num_rows;
    $p=getMaxParcheggio();
    $conn->close();
    return $p-$num_p;

  }
  //ritorna i posti disponibili per il transito
  function postiT(){
    $conn=db_connection();
   
    
   $query="SELECT *
            from Macchina
            where Transito=1";
    $rows=$conn->query($query);
    if(!$rows){
      echo "errore ";
        exit(0);
    }
    $num_p=$rows->num_rows;
    $p=getTraffico();
    $conn->close();
    return $p-$num_p;
  }
  //ritorna i posti disponibili per l'ospedale
  function postiOsp(){
    $conn=db_connection();
    $query="SELECT *
            from Parcheggio
            where occupato=1 and idParcheggio>='C0'";
    $rows=$conn->query($query);
    if(!$rows){
      echo "errore ";
        exit(0);
    }
    $num_p=$rows->num_rows;
    $p=getMaxParcheggio();
    $conn->close();
    return $p-$num_p;
  }
 
?>
