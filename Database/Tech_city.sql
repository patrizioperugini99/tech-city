-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Mag 28, 2021 alle 17:39
-- Versione del server: 10.4.18-MariaDB
-- Versione PHP: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Tech_city`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Macchina`
--

CREATE TABLE `Macchina` (
  `targa` varchar(10) COLLATE utf8mb4_swedish_ci NOT NULL,
  `idPrenotazione` varchar(10) COLLATE utf8mb4_swedish_ci NOT NULL,
  `idParcheggio` varchar(3) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `Transito` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `Parcheggio`
--

CREATE TABLE `Parcheggio` (
  `idParcheggio` varchar(3) COLLATE utf8mb4_swedish_ci NOT NULL,
  `occupato` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Dump dei dati per la tabella `Parcheggio`
--

INSERT INTO `Parcheggio` (`idParcheggio`, `occupato`) VALUES
('A0', 0),
('A1', 0),
('A10', 0),
('A11', 0),
('A12', 0),
('A13', 0),
('A14', 0),
('A15', 0),
('A16', 0),
('A17', 0),
('A18', 0),
('A19', 0),
('A2', 0),
('A3', 0),
('A4', 0),
('A5', 0),
('A6', 0),
('A7', 0),
('A8', 0),
('A9', 0),
('B0', 0),
('B1', 0),
('B10', 0),
('B11', 0),
('B12', 0),
('B13', 0),
('B14', 0),
('B15', 0),
('B16', 0),
('B17', 0),
('B18', 0),
('B19', 0),
('B2', 0),
('B3', 0),
('B4', 0),
('B5', 0),
('B6', 0),
('B7', 0),
('B8', 0),
('B9', 0),
('C0', 0),
('C1', 0),
('C10', 0),
('C11', 0),
('C12', 0),
('C13', 0),
('C14', 0),
('C15', 0),
('C16', 0),
('C17', 0),
('C18', 0),
('C19', 0),
('C2', 0),
('C3', 0),
('C4', 0),
('C5', 0),
('C6', 0),
('C7', 0),
('C8', 0),
('C9', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `Persona`
--

CREATE TABLE `Persona` (
  `nome` varchar(20) COLLATE utf8mb4_swedish_ci NOT NULL,
  `congome` varchar(20) COLLATE utf8mb4_swedish_ci NOT NULL,
  `idPrenotazione` varchar(10) COLLATE utf8mb4_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `Prenotazione`
--

CREATE TABLE `Prenotazione` (
  `idPrenotazione` varchar(10) COLLATE utf8mb4_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Macchina`
--
ALTER TABLE `Macchina`
  ADD PRIMARY KEY (`targa`),
  ADD UNIQUE KEY `idParcheggio` (`idParcheggio`),
  ADD UNIQUE KEY `idParcheggio_2` (`idParcheggio`),
  ADD KEY `idPrenotazione_fk` (`idPrenotazione`);

--
-- Indici per le tabelle `Parcheggio`
--
ALTER TABLE `Parcheggio`
  ADD PRIMARY KEY (`idParcheggio`);

--
-- Indici per le tabelle `Persona`
--
ALTER TABLE `Persona`
  ADD PRIMARY KEY (`nome`,`congome`,`idPrenotazione`),
  ADD KEY `prenotazione_fk` (`idPrenotazione`);

--
-- Indici per le tabelle `Prenotazione`
--
ALTER TABLE `Prenotazione`
  ADD PRIMARY KEY (`idPrenotazione`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `Macchina`
--
ALTER TABLE `Macchina`
  ADD CONSTRAINT `idParcheggio_fk` FOREIGN KEY (`idParcheggio`) REFERENCES `Parcheggio` (`idParcheggio`) ON DELETE CASCADE,
  ADD CONSTRAINT `idPrenotazione_fk` FOREIGN KEY (`idPrenotazione`) REFERENCES `Prenotazione` (`idPrenotazione`) ON DELETE CASCADE;

--
-- Limiti per la tabella `Persona`
--
ALTER TABLE `Persona`
  ADD CONSTRAINT `prenotazione_fk` FOREIGN KEY (`idPrenotazione`) REFERENCES `Prenotazione` (`idPrenotazione`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
