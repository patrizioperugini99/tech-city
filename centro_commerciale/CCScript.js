/*=======================================================================================*/
/*FUNZIONI PER LA TERZA PAGINA*/
var slideIndex = 1;
showSlides(slideIndex);

// cambia immagine visualizzata
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// mostra immagine corrente
function currentSlide(n) {
  showSlides(slideIndex = n);
}
// mostra immagine n
function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}


/*=======================================================================================*/
/*FUNZIONI PER LA QUARTA PAGINA*/
// Apre visualizzazione più grande
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

// chiude visualizzazione più grande
function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex2 = 1;
showSlides2(slideIndex2);

// cambia immagine visualizzata
function plusSlides2(n) {
  showSlides2(slideIndex2 += n);
}

// mostra immagine corrente
function currentSlide2(n) {
  showSlides2(slideIndex2 = n);
}
// mostra immagine n
function showSlides2(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides2");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex2 = 1}
  if (n < 1) {slideIndex2 = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex2-1].style.display = "block";
  dots[slideIndex2-1].className += " active";
  captionText.innerHTML = dots[slideIndex2-1].alt;
}

// torna indietro
function goBack() {
  window.location = document.referrer + '?index=1';
}
