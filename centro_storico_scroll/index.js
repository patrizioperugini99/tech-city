//SLIDESHOW
var slideIndex = 1;
showSlides(slideIndex);

// Precedente/Successivo
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// mostra l'immagine n
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}

//setta visibile il div con la descrizione giusta
function setDivVisible(e){
  var x = e.target.id;
  var hide=document.getElementsByClassName("c1");
  for(i = 0;i<hide.length; i++){
    hide[i].style.display="none";
  }
  if(x=="col"){   
    document.getElementById("colosseop").style.display="block";
  }
  if(x=="mon2"){
    document.getElementById("monumento2p").style.display="block";
  }
  if(x=="mon3"){
    document.getElementById("monumento3p").style.display="block";
  }
}

function assegnaEventHandler(){
  var divDoc=document.getElementsByClassName("load");
  for (i=0;i<divDoc.length;i++)
  divDoc[i].addEventListener("click",setDivVisible);
}

// torna indietro
function goBack() {
 
  window.location = document.referrer + '?index=1';
  
}
